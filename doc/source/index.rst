.. pg_jsonb_flattener documentation master file, created by
   sphinx-quickstart on Sun Dec  9 16:28:58 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pg_jsonb_flattener's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart
   reference


.. automodule:: pg_jsonb_flattener


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
